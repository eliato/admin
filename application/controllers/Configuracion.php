
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 *
 * @extends CI_Controller
 */
class Configuracion extends CI_Controller {

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

		parent::__construct();
		if (!$this->session->userdata("login")) {
			redirect(base_url());
		}
		$this->load->model('user_model');
	    $this->load->library(array('session'));

	}


	public function index() {
			
				$username= $this->session->userdata("nombre");
				$user_id = $this->user_model->get_user_id_from_username($username);
				$user    = $this->user_model->get_opciones_menu_lateral($user_id,1);

								
				$data->msj= $user;
				$this->load->view('inicio/header');
				$this->load->view('inicio/inicio',$data);
				$this->load->view('footer');
				

	}



	

}
