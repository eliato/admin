
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 *
 * @extends CI_Controller
 */
class Mantenimientos extends CI_Controller {

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

		parent::__construct();
		if (!$this->session->userdata("login")) {
			redirect(base_url());
		}
		$this->load->model('user_model');
		$this->load->model('mantenimientos_model');
	    $this->load->library(array('session'));
		$this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
		

	}


	public function index() {
			
				$username= $this->session->userdata("nombre");
				$user_id = $this->user_model->get_user_id_from_username($username);
				$user    = $this->user_model->get_opciones_menu_lateral($user_id,3);

				$data->msj= $user;
				$this->load->view('inicio/header');
				$this->load->view('menu_lateral',$data);
				$this->load->view('footer');
				

		}

	
	
	
	public function products_management(){
				$title['titulo']="Productos";
				$username= $this->session->userdata("nombre");
				$user_id = $this->user_model->get_user_id_from_username($username);
				$user    = $this->user_model->get_opciones_menu_lateral($user_id,3);
				$data->msj= $user; 
				$pro->productos=$this->mantenimientos_model->get_products();

				$this->load->view('inicio/header',$title);
				$this->load->view('menu_lateral',$data);
				$this->load->view('mantenimientos/products_management',$pro);				
				$this->load->view('footer');
				$this->load->view('mantenimientos/product_table');	
		}


		

	public function stock_management(){
				$username= $this->session->userdata("nombre");
				$user_id = $this->user_model->get_user_id_from_username($username);
				$user    = $this->user_model->get_opciones_menu_lateral($user_id,3);
				$data->msj= $user;

				$this->load->view('inicio/header');
				$this->load->view('menu_lateral',$data);
				$this->load->view('mantenimientos/stock_management');
				$this->load->view('footer');
		}

	public function category_management(){
				$username= $this->session->userdata("nombre");
				$user_id = $this->user_model->get_user_id_from_username($username);
				$user    = $this->user_model->get_opciones_menu_lateral($user_id,3);
				$data->msj= $user;
                $category->categorias=$this->mantenimientos_model->get_category();
				$this->load->view('inicio/header');
				$this->load->view('menu_lateral',$data);
				$this->load->view('mantenimientos/category_management',$category);
				$this->load->view('footer');
        $this->load->view('mantenimientos/category_table');

    }


	public function add_product(){

		$nombre = $this->input->post("pro_nombre");
		$codigo = $this->input->post("pro_codigo");

		$this->form_validation->set_rules('pro_nombre', 'Nombre', 'required|min_length[5]');
		$this->form_validation->set_rules('pro_codigo', 'Codigo', 'required|min_length[4]');
		
		
		if ($this->form_validation->run() == FALSE){
			
			redirect(base_url()."mantenimientos/products_management");
			//$this->session->dat("msj");
		}else{
			$data = array(
				"pro_nombre"=>$nombre,
				"pro_codigo"=>$codigo,
				
			);

			//print_r($data);
			
			$this->mantenimientos_model->save_product($data);
			$this->session->set_flashdata('success', 'Se guardo los datos correctamente');
			redirect(base_url()."mantenimientos/products_management");
		}

	}

    public function add_category(){

        $nombre = strtoupper($this->input->post("cat_nombre"));
        $this->form_validation->set_rules('cat_nombre', 'Nombre', 'required|min_length[5]');

        if ($this->form_validation->run() == FALSE){
            redirect(base_url()."mantenimientos/category_management");
            //$this->session->dat("msj");
        }else{
            $data = array(
                "cat_nombre"=>$nombre,
            );

            //print_r($data);

            $this->mantenimientos_model->save_category($data);
            $this->session->set_flashdata('success', 'Se guardo los datos correctamente');
            redirect(base_url()."mantenimientos/category_management");
        }

    }
	

}
