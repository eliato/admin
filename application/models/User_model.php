<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 * 
 * @extends CI_Model
 */
class User_model extends CI_Model {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		
	}
	
	/**
	 * create_user function.
	 * 
	 * @access public
	 * @param mixed $username
	 * @param mixed $email
	 * @param mixed $password
	 * @return bool true on success, false on failure
	 */
	public function create_user($username, $email, $password) {
		
		$data = array(
			'username'   => $username,
			'email'      => $email,
			'password'   => $this->hash_password($password),
			'created_at' => date('Y-m-j H:i:s'),
		);
		
		return $this->db->insert('users', $data);
		
	}
	
	/**
	 * resolve_user_login function.
	 * 
	 * @access public
	 * @param mixed $username
	 * @param mixed $password
	 * @return bool true on success, false on failure
	 */
	public function resolve_user_login($username, $password) {
		
		$this->db->select('password');
		$this->db->from('users');
		$this->db->where('username', $username);
		$hash = $this->db->get()->row('password');
		
		return $this->verify_password_hash($password, $hash);
		
	}
	
	/**
	 * get_user_id_from_username function.
	 * 
	 * @access public
	 * @param mixed $username
	 * @return int the user id
	 */
	public function get_user_id_from_username($username) {
		
		$this->db->select('id_user');
		$this->db->from('users');
		$this->db->where('username', $username);

		return $this->db->get()->row('id_user');
		
	}
	
	/**
	 * get_user function.
	 * 
	 * @access public
	 * @param mixed $user_id
	 * @return object the user object9
	 */
	public function get_user($user_id) {
		
		$this->db->from('users');
		$this->db->where('id_user', $user_id);
		return $this->db->get()->row();
		
	}
	
	/**
	 * hash_password function.
	 * 
	 * @access private
	 * @param mixed $password
	 * @return string|bool could be a string on success, or bool false on failure
	 */
	private function hash_password($password) {
		
		return password_hash($password, PASSWORD_BCRYPT);
		
	}
	
	/**
	 * verify_password_hash function.
	 * 
	 * @access private
	 * @param mixed $password
	 * @param mixed $hash
	 * @return bool
	 */
	private function verify_password_hash($password, $hash) {
		
		return password_verify($password, $hash);
		
	}


	
	public function get_opciones_menu_principal($username) {
		
		$this->db->select('opc_nombre,opc_icono,opc_funcion');
		$this->db->from('users');
		$this->db->join('uxr_usuarioxrol','uxr_id_user=id_user');
		$this->db->join('oxr_opcionxrol','oxr_id_rol=uxr_id_rol');
		$this->db->join('opc_opcion','oxr_id_opc=opc_id');
		$this->db->where('id_user', $username);
		$this->db->where('opc_estado', 1);
		$this->db->where('opc_padre', 0);
		$query = $this->db->get(); 
		 if($query->num_rows() != 0)
		    {
		        return $query->result_array();
		    }
		    else
		    {
		        return false;
		    }
		
	 }

	 public function get_opciones_menu_lateral($username,$padre) {
		
		$this->db->select('opc_nombre,opc_icono,opc_funcion,opc_hijo,opc_id,opc_descripcion');
		$this->db->from('users');
		$this->db->join('uxr_usuarioxrol','uxr_id_user=id_user');
		$this->db->join('oxr_opcionxrol','oxr_id_rol=uxr_id_rol');
		$this->db->join('opc_opcion','oxr_id_opc=opc_id');
		$this->db->where('id_user', $username);
		$this->db->where('opc_estado', 1);
		$this->db->where('opc_padre', $padre);
		$this->db->where('opc_hijo', 0);
		$query = $this->db->get(); 
		 if($query->num_rows() != 0)
		    {
		        return $query->result_array();
		    }
		    else
		    {
		        return false;
		    }
		
	 }


	  public function get_opciones_menu($padre) {
		
		$this->db->select('opc_nombre,opc_icono,opc_funcion,opc_hijo,opc_id');
		$this->db->from('users');
		$this->db->join('uxr_usuarioxrol','uxr_id_user=id_user');
		$this->db->join('oxr_opcionxrol','oxr_id_rol=uxr_id_rol');
		$this->db->join('opc_opcion','oxr_id_opc=opc_id');		
		$this->db->where('opc_estado', 1);
		$this->db->where('opc_padre', $padre);
		$this->db->where('opc_hijo', 1);
		$this->db->order_by('opc_orden','ASC');
		$query = $this->db->get(); 
		 if($query->num_rows() != 0)
		    {
		        return $query->result_array();
		    }
		    else
		    {
		        return false;
		    }
		
	 }


}
