<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 * 
 * @extends CI_Model
 */
class Mantenimientos_model extends CI_Model {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		
	}


public function get_products() {
		
		$this->db->select();
		$this->db->from('pro_productos');
		$this->db->join('cat_categoria','pro_id_cat=cat_id');
		$this->db->join('uni_medida','pro_id_uni=uni_id');
		$query = $this->db->get(); 
		 if($query->num_rows() != 0)
		    {
		        return $query->result_array();
		    }
		    else
		    {
		        return false;
		    }
		
	 }

    public function get_category() {

        $this->db->select();
        $this->db->from('cat_categoria');
        $query = $this->db->get();
        if($query->num_rows() != 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }

    }

    public function save_product($data) {
        $this->db->insert("pro_productos",$data);
    }

    public function save_category($data) {
        $this->db->insert("cat_categoria",$data);
    }














}

