   
        <div class="container-fluid">

			 <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">PRODUCTOS CARGADOS</h3>
                       
                    </div>
                  
                </div>

			<div class="card">
            <div class="card-body">
                <h4 class="card-title">Productos </h4>
                <div class="dt-buttons">
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal" data-whatever="@fat">Agregar Producto  </button>
                </div>
               
                <div class="table-responsive m-t-40">
                    <table id="products_management" name="products_management" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Codigo</th>
                                <th>Categoria</th>
                                <th>Precio Vta</th>
                                <th>Precio Com</th>
                                <th>Stock</th>
                                <th>Medida</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($productos as $key ): ?>
                                <tr>
                                    <td><?php echo $key['pro_nombre'] ?></td>
                                    <td><?php echo $key['pro_codigo'] ?></td>
                                    <td><?php echo $key['cat_nombre'] ?></td>
                                    <td><?php echo $key['pro_precio_vta'] ?></td>
                                    <td><?php echo $key['pro_precio_compra'] ?></td>
                                    <td><?php echo $key['pro_stock'] ?></td>
                                    <td><?php echo $key['uni_nombre'] ?></td>
                                </tr>
                            <?php endforeach ?>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
   </div>
  
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Agregar Producto</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <?php echo validation_errors(); ?>
            <?php echo form_open('mantenimientos/add_product'); ?>
            <div class="modal-body">
                <form  class="form-horizontal" novalidate class="m-t-40">
                    <div class="form-group">
                        <div class="controls">
                        <label for="recipient-name" class="control-label">Nombre:</label>
                        <input type="text" class="form-control <?php echo form_error('pro_nombre') ? 'is-invalid':'' ; ?>" value="<?php echo set_value('pro_nombre'); ?>" id="pro_nombre" name="pro_nombre"  data-validation-required-message="Ingrese un Nombre">
                        <div class="invalid-feedback">
                            <?php echo form_error('pro_nombre'); ?>
                        </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="controls">
                        <label for="recipient-name" class="control-label">Codigo:</label>
                    <input type="text" class="form-control <?php echo form_error('pro_codigo') ? 'is-invalid':'' ; ?>" id="pro_codigo" name="pro_codigo"
                    >
                    <div class="invalid-feedback">
                            <?php echo form_error('pro_codigo'); ?>
                        </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="controls">
                        <label for="recipient-name" class="control-label">Categoria:</label>
                        <select class="custom-select form-control required" id="wlocation2" name="location" required data-validation-required-message="seleccione una opcion" >
                        <option value="">Select City</option>
                        <option value="India">India</option>
                        <option value="USA">USA</option>
                        <option value="Dubai">Dubai</option>
                    </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label">Message:</label>
                        <textarea class="form-control" id="message-text1"></textarea>
                    </div>
                
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Send message</button>
            </div>
            </form>
        </div>
    </div>
</div>
