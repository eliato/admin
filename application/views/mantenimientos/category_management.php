
<div class="container-fluid">

    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">CATEGORIAS</h3>

        </div>

    </div>

    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Categorias </h4>
            <div class="dt-buttons">
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal" data-whatever="@fat">Agregar Categoria  </button>
            </div>

            <div class="table-responsive m-t-40">
                <table id="products_management" name="products_management" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Estado</th>
                        <th>Acciones</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($categorias as $key ): ?>
                        <tr>
                            <td><?php echo $key['cat_nombre'] ?></td>
                            <td><?php echo ($key['cat_estado'] == 1)? "Activo": "Inactivo"; ?></td>
                            <td><button type="button" class="btn waves-effect waves-light btn-rounded btn-success" onclick="edit_category()">Editar</button></td>
                        </tr>
                    <?php endforeach ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Agregar Categoria</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>

            <?php echo validation_errors(); ?>
            <?php echo form_open('mantenimientos/add_category'); ?>
            <div class="modal-body">
                <form  class="form-horizontal" novalidate class="m-t-40">
                    <div class="form-group">
                        <div class="controls">
                            <label for="recipient-name" class="control-label">Nombre:</label>
                            <input type="text" class="form-control <?php echo form_error('cat_nombre') ? 'is-invalid':'' ; ?>" value="<?php echo set_value('pro_nombre'); ?>" id="cat_nombre" name="cat_nombre"  data-validation-required-message="Ingrese un Nombre">
                            <div class="invalid-feedback">
                                <?php echo form_error('cat_nombre'); ?>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
            </form>
        </div>
    </div>
</div>
<script>
    function edit_category() {
        alert("hola");

    }
</script>